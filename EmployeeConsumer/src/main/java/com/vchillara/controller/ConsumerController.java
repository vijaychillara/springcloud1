package com.vchillara.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import com.vchillara.services.Employee;
import com.vchillara.services.RemoteCallService;

public class ConsumerController {
	
	
	/*
	 * @Autowired private LoadBalancerClient lbClient;
	 */

	
	  @Autowired private DiscoveryClient lbClient;
	  
	
	/*
	 * @Autowired private RemoteCallService lbClient;
	 */
	
	public void getEmployee() throws RestClientException, IOException {
		
	
		
		  List<ServiceInstance> instances = lbClient.getInstances("employee-zuul-service"); 
		  ServiceInstance inst = instances.get(0);
		  String  baseURL = inst.getUri().toString() + "/prod/employee";
		  System.out.println("Vijay Chillara..." + baseURL); 
		  RestTemplate restTemplate  = new RestTemplate(); ResponseEntity<String> response = null; try { response=
		  restTemplate.exchange(baseURL, HttpMethod.GET, getHeaders(), String.class);
		  
		  }catch(Exception e) { System.out.println(e); }
		  System.out.println(response.getBody());
		 
		
		/*
		 * try { System.out.println("Vijay Chillara Calling REST API "); Employee emp =
		 * lbClient.getData();
		 * 
		 * System.out.println("Printing Emp Date : " + emp.getName() + "," +
		 * emp.getDesignation());
		 * 
		 * } catch(Exception e) { System.out.println(e); }
		 */	
		
	}

	private static HttpEntity<?> getHeaders()  throws IOException {
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept",MediaType.APPLICATION_JSON_VALUE);
		return new HttpEntity<>(headers);
	
	}

}
