package com.vchillara;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestClientException;

import com.vchillara.controller.ConsumerController;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class EmployeeConsumerApplication {

	public static void main(String[] args) throws RestClientException, IOException{
		ApplicationContext ctx = SpringApplication.run(EmployeeConsumerApplication.class, args);
		
		ConsumerController consumer= ctx.getBean(ConsumerController.class);
		System.out.println(consumer);
		consumer.getEmployee();

		
	 }
	
	@Bean
	public  ConsumerController  ConsumerController()
	{
		return  new ConsumerController();
	}


}
