package com.vchillara.services;


import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name="employee-producer")
public interface RemoteCallService {
	
	@RequestMapping(method=RequestMethod.GET, value="/employee")
	public Employee getData();

}
