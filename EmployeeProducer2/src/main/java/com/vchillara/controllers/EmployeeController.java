package com.vchillara.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.vchillara.model.Employee; 

@RestController
public class EmployeeController {

	@RequestMapping(value="/employee", method=RequestMethod.GET)
	@HystrixCommand(fallbackMethod="getDataFallBack")
	public Employee firstPage() {
		
		Employee emp= new Employee();
		emp.setName("emp1");
		emp.setId("1");
		emp.setDesignation("Software Engineer");
		emp.setSalary(1000);
		
		/*
		 * if(emp.getName().equalsIgnoreCase("emp1")) throw new RuntimeException();
		 */
		return emp;
		
	}
	
	public Employee getDataFallBack() {
		Employee emp= new Employee();
		emp.setName("fallbackemp1");
		emp.setId("1");
		emp.setDesignation("fallbackDesignation");
		emp.setSalary(1000);
		

		return emp;
		
	}
}
